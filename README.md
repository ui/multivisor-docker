A containerised version of [multivisor](https://github.com/tiagocoutinho/multivisor), the 
environment variable `SUPERVISORS` specifies which supervisors to monitor (these should
be semi-colon separated), e.g:

```bash
docker run --name multivisor \
    -e SUPERVISORS="myhost:9034;myhost2:9033" \
    -p 22000:22000 \
    esrfbcu/multivisor
```
