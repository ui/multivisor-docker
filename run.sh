#!/bin/bash

. /opt/conda/etc/profile.d/conda.sh
conda activate multivisor

set -e

echo -e "[global]\nname=Multivisor\n" > multivisor.conf
IFS=';' read -ra supervisors_array <<< "$SUPERVISORS"
for i in "${supervisors_array[@]}"
do
    IFS=':' read -ra details <<< "$i"
    echo -e "[supervisor:${details[0]}]\nurl=$i\n" >> multivisor.conf 
done

multivisor -c ./multivisor.conf
