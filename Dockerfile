# docker build -t multivisor
FROM continuumio/miniconda3:4.10.3 AS main

# Set timezone
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Setup environment
RUN conda config --prepend channels conda-forge
RUN conda update --yes --all && \
    conda create --yes --name multivisor && \
    conda clean --all

RUN ["/bin/bash", "-c", ". /opt/conda/etc/profile.d/conda.sh && \
    pip install supervisor multivisor[web]"]

RUN echo "conda activate multivisor" >> ~/.bashrc

# Run
ENTRYPOINT ["/run.sh"]
COPY run.sh /

EXPOSE 22000
